# -*- coding: utf-8 -*-
import urllib2, re, xbmc, xbmcgui, xbmcaddon, xbmcplugin, os, urllib, urllib2, socket, math, operator, base64, sys

try:
    import json
except ImportError:
    import simplejson as json

socket.setdefaulttimeout(12)

namespace = sys.argv[0]
handler = int(sys.argv[1])

icon = xbmc.translatePath('icon.png')
siteUrl = 'www.filebase.ws'
baseUrl = 'http://' + siteUrl
httpSiteUrl = baseUrl + '/vod/'
videoUrl = 'http://api.streamvodcloud.com/index/'
addon = xbmcaddon.Addon()


def add_menu_item(mode, name, page='1', isFolder=True):
    li = xbmcgui.ListItem(name)
    url = namespace + '?mode=' + mode + '&page=' + page
    xbmcplugin.addDirectoryItem(handler, url, li, isFolder)


def add_open_menu_item(name, img, href, desc, isLisence=False, isFolder=True):
    showName = name + ' - ' + desc
    if isLisence:
        showName += ' - Лицензия'
    li = xbmcgui.ListItem(showName, iconImage=img, thumbnailImage=img)
    url = namespace + '?mode=OPEN&name=%s' % urllib.quote_plus(showName)
    url += '&url=%s' % urllib.quote_plus(baseUrl + href)
    url += '&img_url=%s' % urllib.quote_plus(img)
    xbmcplugin.addDirectoryItem(handler, url, li, isFolder)


def add_play_menu_item(name, img, url):
    li = xbmcgui.ListItem(name, name, iconImage=img, thumbnailImage=img)
    li.setProperty('IsPlayable', 'true')
    xbmcplugin.addDirectoryItem(handler, url, li)


def set_end_of_menu():
    xbmcplugin.endOfDirectory(handler)


def showMessage(heading, message, times=5000):
    xbmc.executebuiltin('XBMC.Notification(%s, %s, %s, %s)' % (heading, message, times, icon))


def get_params(paramstring):
    param = []
    if len(paramstring) >= 2:
        params = paramstring
        cleanedparams = params.replace('?', '')
        if (params[len(params) - 1] == '/'):
            params = params[0:len(params) - 2]
        pairsofparams = cleanedparams.split('&')
        param = {}
        for i in range(len(pairsofparams)):
            splitparams = {}
            splitparams = pairsofparams[i].split('=')
            if (len(splitparams)) == 2:
                param[splitparams[0]] = splitparams[1]
    return param


def GET(url):
    try:
        print('def GET(%s):' % url)
        req = urllib2.Request(url)
        req.add_header('User-Agent',
                       'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36')
        req.add_header('DNT', '1')
        f = urllib2.urlopen(req)
        a = f.read()
        f.close()
        return a
    except:
        # showMessage('Не могу открыть URL def GET', url)
        return None


class BaseMenuFactory:
    mode = ''
    items = {}

    def __init__(self, params=[]):
        self.params = params

    def show_only_licence(self):
        return addon.getSetting('show_only_licence') == 'true'

    def build(self):
        for m, n in self.items.iteritems():
            add_menu_item(m, n)
        set_end_of_menu()


class OpenMenuFactory(BaseMenuFactory):
    mode = 'OPEN'

    def build(self):
        xbmcplugin.setContent(handler, 'movies')
        url = urllib.unquote_plus(self.params['url'])
        name = urllib.unquote_plus(self.params['name'])
        img = urllib.unquote_plus(self.params['img_url'])
        videoId = url[url.rindex('=') + 1:]

        httpWatch = GET(url)

        if httpWatch is not None:
            r1 = re.compile('var\s*datas\s*=\s*{\s*"file_type":file_type,\s*"quality":quality,\s*"st":"(.*?)",\s*"e":"(.*?)"', re.S).findall(
                httpWatch)
            hlsUrl = videoUrl + videoId + '?file_type=hls&st=' + r1[0][0] + '&e=' + r1[0][1]+'&quality=720'
            httpHls = GET(hlsUrl)

            if httpHls is not None and '"playlist":[]' not in httpHls:
                if 'playlist' in httpHls:
                    self.playlist(name, img, httpHls)
                else:
                    self.single(name, img, httpHls)
            else:
                hlsUrl = videoUrl + videoId + '?file_type=hls&st=' + r1[0][0] + '&e=' + r1[0][1]+'&quality=420'
                httpHls = GET(hlsUrl)
                if httpHls is not None and '"playlist":[]' not in httpHls:
                    if 'playlist' in httpHls and '"playlist":[]' not in httpHls:
                        self.playlist(name, img, httpHls)
                    else:
                        self.single(name, img, httpHls)
                else:
                    hlsUrl = videoUrl + videoId + '?file_type=hls&st=' + r1[0][0] + '&e=' + r1[0][1]+'&quality=360'
                    httpHls = GET(hlsUrl)
                    if httpHls is not None and '"playlist":[]' not in httpHls:
                        if 'playlist' in httpHls:
                            self.playlist(name, img, httpHls)
                        else:
                            self.single(name, img, httpHls)

            set_end_of_menu()

    def single(self, name, img, httpHls):
        r2 = re.compile('\|(http://.*?)$', re.S).findall(httpHls)
        videoHls = r2[0]
        add_play_menu_item(name, img, videoHls)

    def playlist(self, name, img, httpHls):
        r2 = re.compile('pl\|{"playlist":(.*?)}$', re.S).findall(httpHls)

        if len(r2) == 0:
            showMessage('Показать нечего', 'Список пуст')
            return False

        print("list=%s" % r2[0])

        items = json.loads(r2[0])

        if len(items) == 0:
            showMessage('Показать нечего', 'Список не найден')
            return False

        for item in items:
            if item.has_key('playlist'):
                for sub_item in item['playlist']:
                    add_play_menu_item(item['comment'] + ' - ' + sub_item['comment'], img, sub_item['file'])
            else:
                add_play_menu_item(item['comment'], img, item['file'])


class ViewMenuFactory(BaseMenuFactory):
    mode_type = ''

    def build(self):
        page = self.params['page']
        url = httpSiteUrl + '?type=' + self.mode_type + '&page=' + page
        http = GET(url)

        if http is not None:
            rNext = re.compile('<a href="\?type=' + self.mode_type + '&page=(.[^"]*?)" title="Следующая страница">',
                               re.S).findall(http)
            rPrev = re.compile('<a href="\?type=' + self.mode_type + '&page=(.[^"]*?)" title="Предыдущая страница">',
                               re.S).findall(http)
            r1 = re.compile('<div class="row movies">(.*?)<center>', re.S).findall(http)
            r2 = re.compile('<div class="item">(.*?)<div class="desc">(.*?)</div>.*?</div>.*?</div>', re.S).findall(
                http)

            if len(rPrev) > 0:
                add_menu_item(RootMenuFactory.mode, 'Главное меню')
                add_menu_item(self.mode, '<< Предыдущая страница', rPrev[0])

            for item, desc in r2:
                r3 = re.compile('<a href="(.*?)"><img.*?title="(.*?)" src="(.*?)".*?</a>(.*?)<div class="info">',
                                re.S).findall(item)
                for href, name, img, label in r3:
                    lsrc = label.strip()
                    if lsrc:
                        r4 = re.compile('"label">(.*?)</div>', re.S).findall(lsrc)
                        if len(r4) > 0:
                            name = name + ' - ' + r4[0]
                    add_open_menu_item(name, img, href, desc)

            if len(rNext) > 0:
                add_menu_item(self.mode, '>> Следующая страница', rNext[0])

            set_end_of_menu()


class FilmsMenuFactory(ViewMenuFactory):
    mode = 'films_sub'
    mode_type = 'films'


class SerialsMenuFactory(ViewMenuFactory):
    mode = 'serials_sub'
    mode_type = 'serials'


class TvMenuFactory(ViewMenuFactory):
    mode = 'tv_sub'
    mode_type = 'tv'


class RootMenuFactory(BaseMenuFactory):
    mode = 'root'
    items = {'films_sub': 'Фильмы', 'serials_sub': 'Сериалы', 'tv_sub': 'Телепередачи'}

    def build(self):
        try:
            mode = urllib.unquote_plus(self.params['mode'])
        except:
            BaseMenuFactory.build(self)
        else:
            factory = None

            if mode == self.mode:
                BaseMenuFactory.build(self)
                return True

            if mode == FilmsMenuFactory.mode:
                factory = FilmsMenuFactory(self.params)

            if mode == SerialsMenuFactory.mode:
                factory = SerialsMenuFactory(self.params)

            if mode == TvMenuFactory.mode:
                factory = TvMenuFactory(self.params)

            if mode == OpenMenuFactory.mode:
                factory = OpenMenuFactory(self.params)

            print(mode)

            factory.build()
