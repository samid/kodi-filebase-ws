#!/usr/bin/python
# -*- coding: utf-8 -*-
#/*
# *      Copyright (C) 2014 by Andrejs Semovs <andrejs.semovs|at|gmail.com>
# *      Copyright (C) 2011 - 2014 by tolin
# *
# *
# *  This Program is free software; you can redistribute it and/or modify
# *  it under the terms of the GNU General Public License as published by
# *  the Free Software Foundation; either version 2, or (at your option)
# *  any later version.
# *
# *  This Program is distributed in the hope that it will be useful,
# *  but WITHOUT ANY WARRANTY; without even the implied warranty of
# *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# *  GNU General Public License for more details.
# *
# *  You should have received a copy of the GNU General Public License
# *  along with this program; see the file COPYING.  If not, write to
# *  the Free Software Foundation, 675 Mass Ave, Cambridge, MA 02139, USA.
# *  http://www.gnu.org/copyleft/gpl.html
# */
import urllib2, re, xbmc, xbmcgui, xbmcplugin, os, urllib, urllib2, socket, math, operator, base64

try:
    import json
except ImportError:
    import simplejson as json

socket.setdefaulttimeout(12)

h = int(sys.argv[1])
#icon = xbmc.translatePath(os.path.join(os.getcwd().replace(';', ''),'icon.png'))
#path     = xbmc.translatePath(xbmcaddon.Addon(id='plugin.video.filmix.net').getAddonInfo('path') )
icon = xbmc.translatePath('icon.png')
siteUrl = 'filmix.net'
httpSiteUrl = 'http://' + siteUrl

def showMessage(heading, message, times = 5000):
    xbmc.executebuiltin('XBMC.Notification("%s", "%s", %s, "%s")'%(heading, message, times, icon))

def GET(url):
	try:
		print 'def GET(%s):'%url
		req = urllib2.Request(url)
		f = urllib2.urlopen(req)
		a = f.read()
		f.close()
		return a
	except:
		showMessage('Не могу открыть URL def GET', url)
		return None
	    
def GETSER(url):
	http = GET(urllib.unquote_plus(params['url']))
#	showMessage('ser url', params['url'])
#	rows1 = re.compile('&amp;pl=(.+?)&amp;').findall(http)
	rows1 = re.compile('var fileArray(.*?)pl5Array',re.S).findall(http)
	#print rows1
	rows2 = re.compile("plname = '(.*?)',").findall(rows1[0])
	url = DEC(rows2[0])
	return url
	
#	if len(rows1) >= 1:
#	    PLAY1(params)
#	else:
#	    r1 = re.compile("<div id='dle-content'>(.*?)<!--/PLAYER--><!-- div players -->", re.S).findall(http)
	
#	if len(r1):
#	    ro2 = re.compile('&amp;pl=(.*?)&amp;',re.S).findall(r1[0])
#	    if len(ro2):
#		pl = DEC(ro2[0])
#		return pl

#	return None


def ROOT():

        name='Фильмы'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=video_sub'
        xbmcplugin.addDirectoryItem(h, url, li, True)
        
        name='Сериалы'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=serial_sub'
        xbmcplugin.addDirectoryItem(h, url, li, True)
	
	name='ТОП по просмотрам'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=top1'+'&url=%s'%urllib.quote_plus('http://filmix.net/?do=top100&mode=views')
        xbmcplugin.addDirectoryItem(h, url, li, True)
	
	
	name='ТОП по комментариям'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=top1'+'&url=%s'%urllib.quote_plus('http://filmix.net/?do=top100&mode=comments')
        xbmcplugin.addDirectoryItem(h, url, li, True)
	
	name='Поиск'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=search'
        xbmcplugin.addDirectoryItem(h, url, li, True)
        		
	xbmcplugin.endOfDirectory(h)


def video_sub():
    
	name='Жанры'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=genre'
        xbmcplugin.addDirectoryItem(h, url, li, True)
    
	name='Фильмы от А-Я, 0-9'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=ROOT3'+'&url=%s'%urllib.quote_plus('http://filmix.net/?do=archive')
        xbmcplugin.addDirectoryItem(h, url, li, True)
        xbmcplugin.endOfDirectory(h)
    
def serial_sub():
    
#	name='Популярные'
#        li = xbmcgui.ListItem(name)
#        url = sys.argv[0] + '?mode=top_view'
#        xbmcplugin.addDirectoryItem(h, url, li, True)
	
	name='Сериалы'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=OPEN_SER'+'&url=%s'%urllib.quote_plus('http://filmix.net/serialy/')
        xbmcplugin.addDirectoryItem(h, url, li, True)

	name='Сериалы от А-Я, 0-9'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=ROOT4'+'&url=%s'%urllib.quote_plus('http://filmix.net/?do=serials')
        xbmcplugin.addDirectoryItem(h, url, li, True)
        xbmcplugin.endOfDirectory(h)
    
def search():
	
	skbd = xbmc.Keyboard()
	skbd.setHeading('Название фильма или часть названия')
	skbd.doModal()

	SearchString = skbd.getText(0)
	url = httpSiteUrl+'/do=search&subaction=search&story='+ SearchString

	http = GET(url)
	if http == None: return False
	r1 = re.compile('<div class="block serials">(.*?)</ul>',re.S).findall(http)
	r2 = re.compile('<a href="(.*?)">(.*?)</a>',re.S).findall(r1[0])
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href, name in r2:
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
	xbmcplugin.endOfDirectory(h)
	
def genre():
	wurl = 'http://filmix.net/'
	http = GET(wurl)
	if http == None: return False
	r1 = re.compile('<div class="block categories">(.*?)</ul>',re.S).findall(http)
	r2 = re.compile('<li><a href="(.*?)">(.*?)</a>',re.S).findall(r1[0])
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href, name in r2:
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES'
#		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
		u += '&url=%s'%urllib.quote_plus(href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
	
	xbmcplugin.endOfDirectory(h)

def top_view():
	wurl = 'http://filmix.net/'
	http = GET(wurl)
	if http == None: return False
	r1 = re.compile('<div class="block serials">(.*?)</ul>',re.S).findall(http)
	r2 = re.compile('<li><a href="(.*?)" target="_blank">(.*?)</a></li>').findall(r1[0])
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href, name in r2:
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES4'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
	
	xbmcplugin.endOfDirectory(h)

	
def ROOT1():
	wurl = 'http://filmix.net/'
	http = GET(wurl)
	if http == None: return False
	r1 = re.compile('<li class="li_year"><a href="(.*?)" class="rollerDate">(.*?)</a></li>').findall(http)
	if len(r1) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href, name in r1:
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
	xbmcplugin.endOfDirectory(h)


def OPEN_MOVIES1(params):
    
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
#	rows = re.compile('<a href="(.+?)"><img src="(.+?)" alt="(.+?)" /></a>').findall(http)
	r1 = re.compile('<div id="container">(.*?)</div><!-- #content-->',re.S).findall(http)
	r2 = re.compile('<h2><a href="(.*?)">(.*?)</a></h2>',re.S).findall(r1[0])
#	#r3 = re.compile('|left--><a href="(.*?)" onclick',re.S).findall(r1[0])
	r3 = re.compile('><img src="(.*?)" style="float:left;" alt=',re.I).findall(r1[0])
	r4 = re.compile('<br />(.*?)</div>',re.S).findall(r1[0])
	
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов href, img, alt')
		return False
	ii = 0
	name='[B][COLOR blue]Жанры[/COLOR][/B]'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=genre'
        xbmcplugin.addDirectoryItem(h, url, li, True)
	for href, alt in r2:
			img = 'http://filmix.net' + r3[ii]
#			img = icon
			text = r4[ii]
			ii = ii + 1
			i = xbmcgui.ListItem(unicode(alt, "cp1251"), iconImage=img, thumbnailImage=img)
			i.setInfo(type='video', infoLabels={'title': unicode(alt, "cp1251"), 'plot': unicode(text, "cp1251")})
#			i = xbmcgui.ListItem(alt, iconImage=img, thumbnailImage=img)
#			i.setInfo(type='video', infoLabels={'title': alt, 'plot': text})
			u  = sys.argv[0] + '?mode=PLAY1'
			u += '&url=%s'%urllib.quote_plus(href)
			#i.setProperty('IsPlayable', 'true')
			xbmcplugin.addDirectoryItem(h, u, i, True)
	try:
		rp = re.compile('<div class="pages">(.*?)</div>', re.DOTALL).findall(http)[0]
		rp2 = re.compile('<a href="(.*?)">(.*?)</a>').findall(rp)
		for href, nr in rp2:
			u = sys.argv[0] + '?mode=OPEN_MOVIES'
			u += '&url=%s'%urllib.quote_plus(href)
#			rPN = '[B][COLOR yellow]%s[/COLOR][/B]' % rPN
			i = xbmcgui.ListItem('[ Страница %s ]'%nr)
			xbmcplugin.addDirectoryItem(h, u, i, True)
	except:
		pass
	xbmcplugin.endOfDirectory(h)
	
	
	
	
	
def OPEN_MOVIES(params):
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
#	rows = re.compile('<a href="(.+?)"><img src="(.+?)" alt="(.+?)" /></a>').findall(http)
	r1 = re.compile("<div id='dle-content'>(.*?)</div><!-- #content-->",re.S).findall(http)
#	r1 = re.compile('<div id="container">(.*?)</div><!-- #content-->',re.S).findall(http)
	r2 = re.compile('<div class="zagolovok"><a href="(.*?)">(.*?)</a></div>',re.S).findall(r1[0])
#	#r3 = re.compile('|left--><a href="(.*?)" onclick',re.S).findall(r1[0])
	r3 = re.compile('><img src="(.*?)" style="float:left;" alt=',re.I).findall(r1[0])
	r4 = re.compile('<br />(.*?)</div>',re.S).findall(r1[0])
	
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов href, img, alt')
		return False
	ii = 0
	name='[B][COLOR blue]Жанры[/COLOR][/B]'
        li = xbmcgui.ListItem(name)
        url = sys.argv[0] + '?mode=genre'
        xbmcplugin.addDirectoryItem(h, url, li, True)
	for href, alt in r2:
			img = 'http://filmix.net' + r3[ii]
#			img = icon
			text = r4[ii]
			ii = ii + 1
			i = xbmcgui.ListItem(unicode(alt, "cp1251"), iconImage=img, thumbnailImage=img)
			i.setInfo(type='video', infoLabels={'title': unicode(alt, "cp1251"), 'plot': unicode(text, "cp1251")})
#			i = xbmcgui.ListItem(alt, iconImage=img, thumbnailImage=img)
#			i.setInfo(type='video', infoLabels={'title': alt, 'plot': text})
			u  = sys.argv[0] + '?mode=PLAY1'
			u += '&url=%s'%urllib.quote_plus(href)
			#i.setProperty('IsPlayable', 'true')
			xbmcplugin.addDirectoryItem(h, u, i, True)
	try:
		rp = re.compile('<div class="pages">(.*?)</div>', re.DOTALL).findall(http)[0]
		rp2 = re.compile('<a href="(.*?)">(.*?)</a>').findall(rp)
		for href, nr in rp2:
			u = sys.argv[0] + '?mode=OPEN_MOVIES'
			u += '&url=%s'%urllib.quote_plus(href)
#			rPN = '[B][COLOR yellow]%s[/COLOR][/B]' % rPN
			i = xbmcgui.ListItem('[ Страница %s ]'%nr)
			xbmcplugin.addDirectoryItem(h, u, i, True)
	except:
		pass
	xbmcplugin.endOfDirectory(h)
	
	
def OPEN_SER(params):
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
#	rows = re.compile('<a href="(.+?)"><img src="(.+?)" alt="(.+?)" /></a>').findall(http)
#	r1 = re.compile('<div id="container">(.*?)</div><!-- #content-->',re.S).findall(http)
	r1 = re.compile("<div id='dle-content'>(.*?)</div><!-- #content-->",re.S).findall(http)
	r2 = re.compile('<div class="zagolovok"><a href="(.*?)">(.*?)</a></div>',re.S).findall(r1[0])
#	#r3 = re.compile('|left--><a href="(.*?)" onclick',re.S).findall(r1[0])
	r3 = re.compile('><img src="(.*?)" style="float:left;" alt=',re.I).findall(r1[0])
	r4 = re.compile('<br />(.*?)</div>',re.S).findall(r1[0])
	r5 = re.compile('<li class="rating">(.*?)</font>', re.S).findall(r1[0])
	
			
	
	if len(r2) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов href, img, alt')
		return False
	ii = 0
#	name='[B][COLOR blue]Жанры[/COLOR][/B]'
#        li = xbmcgui.ListItem(name)
#        url = sys.argv[0] + '?mode=genre'
#        xbmcplugin.addDirectoryItem(h, url, li, True)
	for href, alt in r2:
			img = 'http://filmix.net' + r3[ii]
			r6 = re.compile('<b>(.*?)</b>').findall(r5[ii])
			r6 = str(r6)			
			r6 = r6[2:len(r6)-2]
#			r6 = r6[:2]
			r6 = '[COLOR ffFFD700]%s[/COLOR]' % r6
			alt = alt + '  ' + r6
#			img = icon
			text = r4[ii]
			ii = ii + 1
			i = xbmcgui.ListItem(unicode(alt, "cp1251"), iconImage=img, thumbnailImage=img)
			i.setInfo(type='video', infoLabels={'title': unicode(alt, "cp1251"), 'plot': unicode(text, "cp1251")})
#			i = xbmcgui.ListItem(alt, iconImage=img, thumbnailImage=img)
#			i.setInfo(type='video', infoLabels={'title': alt, 'plot': text})
			u  = sys.argv[0] + '?mode=OPEN_MOVIES4'
			u += '&url=%s'%urllib.quote_plus(href)
			u += '&name=%s'%urllib.quote_plus(alt)
			
			xbmcplugin.addDirectoryItem(h, u, i, True)
			
			
	try:
		rp = re.compile('<div class="pages">(.*?)</div>', re.DOTALL).findall(http)[0]
		rp2 = re.compile('<a href="(.*?)">(.*?)</a>').findall(rp)
		for href, nr in rp2:
			u = sys.argv[0] + '?mode=OPEN_SER'
			u += '&url=%s'%urllib.quote_plus(href)
#			rPN = '[B][COLOR yellow]%s[/COLOR][/B]' % rPN
			i = xbmcgui.ListItem('[ Страница %s ]'%nr)
			xbmcplugin.addDirectoryItem(h, u, i, True)
	except:
		pass
	xbmcplugin.endOfDirectory(h)


def ROOT3():
#	wurl = 'http://filmix.net'
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
#	showMessage('root3 url', params['url'])
	rs0 = re.compile('<ul class="catalog">(.*?)<div class="sidebar" id="sideLeft">', re.DOTALL).findall(http)
#	r1 = re.compile('<li><a href="(.*?)">(.*?)</a></li>').findall(http)
#    rs0 = re.compile('<ul class="catalog">(.*?)<div class="sidebar" id="sideLeft">', re.DOTALL).findall(http)
#	rs = re.compile('<div class="full-link"><a href="(.+?)" class="showfull">(.+?)</a></div>\s*<div class="letter">(.+?)</div>').findall(rs0[0])
#	rs1 = re.compile('<li><a href="(.*?)">.*?</a></li>').findall(rs0[0])
	rs1 = re.compile('<div class="full-link">\s*<a href="(.*?)">.*?</a>').findall(rs0[0])
	rs2 = re.compile('<div class="letter">(.*?)</div>').findall(rs0[0])
	io = 0
	if len(rs1) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href in rs1:
		name = rs2[io]
		io = io + 1
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(unicode(name, "cp1251") + ' ' + unicode(name1, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name + ' ' + name1, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES2'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
#		u += '&url=%s'%urllib.quote_plus(href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
		#showMessage('root3', u)
	xbmcplugin.endOfDirectory(h)


def top1():
#	wurl = 'http://filmix.net'
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
	rs0 = re.compile('<table class="generic">(.*?)</table>', re.DOTALL).findall(http)
	rs1 = re.compile('<tr>\s*<td>(.*?)</td>').findall(rs0[0])
	rs2 = re.compile('<a href="(.*?)">(.*?)</a>').findall(rs0[0])
	rs3 = re.compile('<td class="right" align="center">(.*?)</td>').findall(rs0[0])
	io = 0
	if len(rs1) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href, name in rs2:
		nr = rs1[io]
		nr2 = rs3[io]
		nr2 = '[COLOR ffFFD700]%s[/COLOR]' % nr2
		io = io + 1
		i = xbmcgui.ListItem(nr + '   ' + unicode(name, "cp1251") + '     ' + nr2, iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name + ' ' + name1, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES4'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net/' + href)
		xbmcplugin.addDirectoryItem(h, u, i, True)
		#showMessage('dss', u)
	xbmcplugin.endOfDirectory(h)


def OPEN_MOVIES2(params):
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
#	showMessage('url mov2', params['url'])
	rows0 = re.compile('<ul class="catalog">(.*?)</ul>', re.DOTALL).findall(http)
#	rows0 = re.compile('<ul class="catalog">(.*?)<div class="sidebar" id="sideLeft">', re.DOTALL).findall(http)
	rows = re.compile('<li><a href="(.+?)">(.+?)</a></li>').findall(rows0[0])
	if len(rows) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов href, img, alt')
		return False
	for href, alt in rows:
			img = icon
			i = xbmcgui.ListItem(unicode(alt, "cp1251"), iconImage=img, thumbnailImage=img)
#			i = xbmcgui.ListItem(alt, iconImage=img, thumbnailImage=img)
			u  = sys.argv[0] + '?mode=PLAY1'
			u += '&url=%s'%urllib.quote_plus('http://filmix.net/' + href)
			#u += '&name=%s'%urllib.quote_plus(name)
			#i.setProperty('IsPlayable', 'true')
			xbmcplugin.addDirectoryItem(h, u, i, True)

	xbmcplugin.endOfDirectory(h)


def DEC(param):
    
    hash1 = ("l","u","T","D","Q","H","0","3","G","1","f","M","p","U","a","I","6","k","d","s","b","W","5","e","y","=");
    hash2 = ("w","g","i","Z","c","R","z","v","x","n","N","2","8","J","X","t","9","V","7","4","B","m","Y","o","L","h");

    for i in range(0, len(hash1)):
        rr1 = hash1[i]
        rr2 = hash2[i]

        param = param.replace(rr1, '--')
        param = param.replace(rr2, rr1)
        param = param.replace('--', rr2)
	
    
    param = base64.b64decode(param)
    
    return param


# Parse given movie url into a list of urls with quality info
def PARSE_MOVIE_URL(url):
	result = []
	rqlt = r"\[([0-9,]*)\]"
	quality = re.compile(rqlt).findall(url)

	if quality is None or len(quality) == 0:
		result.append({'url': url, 'definition': '', 'quality': ''})

	else:
		# trim leading , if any and split into list
	    quality = quality[0].strip(',').split(',')

	    # cycle through available qualities
	    for bps in quality:
	    	# prepare url with given bps
	        parsed_url = re.sub(rqlt, bps, url)

	        # generate SD or HD string for menu title
	        suffix = 'SD' if int(bps) < 720 else 'HD'

	        # populate result with parsed url, definition and quality
	        result.append({'url': parsed_url, 'definition': suffix, 'quality': bps})

	return result


def PLAY1(params):
	http = GET(urllib.unquote_plus(params['url']))
	showMessage('url in Play', params['url'])
	if http is None:
		return False

#	rows1 = re.compile('&amp;file=(.+?)&amp;').findall(http)
	rows1 = re.compile('var fileArray(.*?)file5Array',re.S).findall(http)
	rows2 = re.compile("'(.*?)'").findall(rows1[0])

	if len(rows2) == 0:
		OPEN_MOVIES4(params)
	else:
		if len(rows2)==1:
		    link = DEC(rows2[0])
		    showMessage('link', link)
		    urls = PARSE_MOVIE_URL(link)
		    OPEN_MOVIE_URLS({'urls': urllib.quote_plus(json.dumps(urls))})
		else:
		    name='Фильм'
		    li = xbmcgui.ListItem(name)
		    link = DEC(rows2[0])
#		    showMessage('len rows1',len(rows1))
		    urls = PARSE_MOVIE_URL(link)
		    url = sys.argv[0] + '?mode=OPEN_MOVIE_URLS' + '&urls=%s'%urllib.quote_plus(json.dumps(urls))
	            xbmcplugin.addDirectoryItem(h, url, li, True)
		    
		    name='Трейлер'
		    li = xbmcgui.ListItem(name)
		    link = DEC(rows2[1])
#		    showMessage('len rows1',len(rows1))
		    urls = PARSE_MOVIE_URL(link)
		    url = sys.argv[0] + '?mode=OPEN_MOVIE_URLS' + '&urls=%s'%urllib.quote_plus(json.dumps(urls)) 
	            xbmcplugin.addDirectoryItem(h, url, li, True)
	            xbmcplugin.endOfDirectory(h)



def ROOT4():
#	wurl = 'http://filmix.net'
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
	rs0 = re.compile('<ul class="catalog">(.*?)<div class="sidebar" id="sideLeft">', re.DOTALL).findall(http)
#	r1 = re.compile('<li><a href="(.*?)">(.*?)</a></li>').findall(http)
#    rs0 = re.compile('<ul class="catalog">(.*?)<div class="sidebar" id="sideLeft">', re.DOTALL).findall(http)
#	rs = re.compile('<div class="full-link"><a href="(.+?)" class="showfull">(.+?)</a></div>\s*<div class="letter">(.+?)</div>').findall(rs0[0])
	rs1 = re.compile('<div class="full-link">\s*<a href="(.*?)">.*?</a>').findall(rs0[0])
	rs2 = re.compile('<div class="letter">(.*?)</div>').findall(rs0[0])
	io = 0
	if len(rs1) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов id,name,link,numberOfMovies')
		return False
	for href in rs1:
		name = rs2[io]
		io = io + 1
		i = xbmcgui.ListItem(unicode(name, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(unicode(name, "cp1251") + ' ' + unicode(name1, "cp1251"), iconImage=icon, thumbnailImage=icon)
#		i = xbmcgui.ListItem(name + ' ' + name1, iconImage=icon, thumbnailImage=icon)
		u  = sys.argv[0] + '?mode=OPEN_MOVIES3'
		u += '&url=%s'%urllib.quote_plus('http://filmix.net' + href)
		u += '&name=%s'%urllib.quote_plus(name)
		xbmcplugin.addDirectoryItem(h, u, i, True)
		#showMessage('dss', u)
	xbmcplugin.endOfDirectory(h)


def OPEN_MOVIES3(params):
	http = GET(urllib.unquote_plus(params['url']))
	if http == None: return False
	rows0 = re.compile('<ul class="catalog">(.*?)</ul>', re.DOTALL).findall(http)
	rows = re.compile('<li><a href="(.+?)">(.+?)</a></li>').findall(rows0[0])
	rr = rows[0]
#	showMessage('rows  mov3', rr)
	if len(rows) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов href, img, alt')
		return False
	for href, alt in rows:
			img = icon
			hhtt = href
			i = xbmcgui.ListItem(unicode(alt, "cp1251"), iconImage=img, thumbnailImage=img)
#			i = xbmcgui.ListItem(alt, iconImage=img, thumbnailImage=img)
			u  = sys.argv[0] + '?mode=OPEN_MOVIES4'
			u += '&url=%s'%urllib.quote_plus('http://filmix.net/' + href)
			xbmcplugin.addDirectoryItem(h, u, i, True)
	xbmcplugin.endOfDirectory(h)


# Open movie list, that is passed in 'urls' parameter (mostly used to show different qualities of a movie)
def OPEN_MOVIE_URLS(params):
	urls = json.loads(urllib.unquote_plus(params['urls']))
#	showMessage('urls', urls)
	# notify if no urls are found
	if len(urls) == 0:
		showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов urls')
		return False

	# otherwise output all quality urls
	for url in urls:

		# show definition and bps
		if 'quality' in url and len(url['quality']):
			title = url['definition'] + ' ' + url['quality']
		# otherwise just use plain video file name
		else:
			title = os.path.basename(url['url'])

		i = xbmcgui.ListItem(title, iconImage=icon, thumbnailImage=icon)
		i.setProperty('IsPlayable', 'true')
		xbmcplugin.addDirectoryItem(h, url['url'], i)

	xbmcplugin.endOfDirectory(h)


# Open serial movie list
def OPEN_MOVIES4(params):
    link = GET(urllib.unquote_plus(params['url']))

    hhh = GETSER(params['url'])

    if hhh is None:
    	showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Ссылка на видео файл не обнаружена', 5000)
    	return False

    # get and decode filmix playlist
    js = DEC(GET(urllib.unquote_plus(hhh)))

    # exit if no data received
    if js == None: return False

    # parse playlist JSON
    playlist = json.loads(js)

    # exit, if playlist empty
    if len(playlist) == 0:
    	showMessage('ПОКАЗАТЬ НЕЧЕГО', 'Нет элементов')
        return False

    # if season level is not found, create it
    if 'file' in playlist['playlist'][0]:
    	playlist = { 'playlist': [ playlist ] }

    # loop through playlist data
    for season in playlist['playlist']:
        for episode in season['playlist']:
			urls = PARSE_MOVIE_URL(episode['file'])

			# if multiple video qualities are found, add folder link
			if len(urls) > 1:
				u  = sys.argv[0] + '?mode=OPEN_MOVIE_URLS'
				u += '&urls=%s' % urllib.quote_plus(json.dumps(urls))
				i = xbmcgui.ListItem(episode['comment'] + ' ...', iconImage=icon, thumbnailImage=icon)
				xbmcplugin.addDirectoryItem(h, u, i, True)

			# otherwise add direct link to video
			else:
				for url in urls:
					title = episode['comment']

					# add quality info to title if available
					if url['quality'] is not None and len(url['quality']):
						title += ' - ' + url['definition'] + ' ' + url['quality']
		
					i = xbmcgui.ListItem(title, iconImage=icon, thumbnailImage=icon)
					i.setProperty('IsPlayable', 'true')
					xbmcplugin.addDirectoryItem(h, url['url'], i)

    xbmcplugin.endOfDirectory(h)


def PLAY(params):
	http = params['url']
#	showMassage ('http play', http)
        img = icon
	i = xbmcgui.ListItem('test', iconImage=img, thumbnailImage=img)
	xbmc.Player().play(http, i)
    #   k = 'http://video-6.filmix.net/s/c8b86969d4e1d410c030d1546130bfdf/1942/e04.flv'
#    i = xbmcgui.ListItem(path = k)
        xbmcplugin.setResolvedUrl(h, True, i)

def get_params(paramstring):
	param=[]
	if len(paramstring)>=2:
		params=paramstring
		cleanedparams=params.replace('?','')
		if (params[len(params)-1]=='/'):
			params=params[0:len(params)-2]
		pairsofparams=cleanedparams.split('&')
		param={}
		for i in range(len(pairsofparams)):
			splitparams={}
			splitparams=pairsofparams[i].split('=')
			if (len(splitparams))==2:
				param[splitparams[0]]=splitparams[1]
	return param

params=get_params(sys.argv[2])


mode = None

try:
	mode = urllib.unquote_plus(params['mode'])
except:
	ROOT()

if mode == 'OPEN_MOVIES': OPEN_MOVIES(params)

if mode == 'OPEN_SER': OPEN_SER(params)

if mode == 'ROOT4': ROOT4()
	
if mode == 'OPEN_MOVIES2': OPEN_MOVIES2(params)
	
if mode == 'OPEN_MOVIES3': OPEN_MOVIES3(params)
	
if mode == 'OPEN_MOVIES4': OPEN_MOVIES4(params)

if mode == 'OPEN_MOVIE_URLS': OPEN_MOVIE_URLS(params)

if mode == 'ROOT3': ROOT3()

if mode == 'video_sub': video_sub()
if mode == 'serial_sub': serial_sub()
if mode == 'search': search()
if mode == 'genre': genre()
if mode == 'top_view': top_view()
if mode == 'PLAY': PLAY(params)
	
if mode == 'PLAY1': PLAY1(params)
if mode == 'top1': top1()


#try:
#	import adanalytics
#	adanalytics.adIO(sys.argv[0], sys.argv[1], sys.argv[2])
#except:
#	xbmc.log(' === unhandled exception in adIO ==== ')
#	pass
