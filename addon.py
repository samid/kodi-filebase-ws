# -*- coding: utf-8 -*- 
import base

params = base.get_params(sys.argv[2])
factory = base.RootMenuFactory(params)
factory.build()